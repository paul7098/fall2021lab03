package LinearAlgebra;

final class Vector3d {
    public double x;
    public double y;
    public double z;

    public static void main(String args[]) {
        Vector3d vect1 = new Vector3d(2, 3, 4);
        Vector3d vect2 = new Vector3d(1, 1, 1);
        Vector3d newVect = vect1.add(vect2);
        System.out.println(newVect.getX());
        System.out.println(newVect.getY());
        System.out.println(newVect.getZ());

        double dot = vect1.dotProduct(vect2);
        System.out.println(dot);

        double mag = vect1.magnitude();
        System.out.println(mag);

    }

    public Vector3d(double x, double y, double z) {
        this.x = x;
        this.y = y;
        this.z = z;
    }

    public double getX() {
        return this.x;
    }

    public double getY() {
        return this.y;
    }

    public double getZ() {
        return this.z;
    }

    public double magnitude() {
        double mag = Math.sqrt(Math.pow(this.x, 2) + Math.pow(this.y, 2) + Math.pow(this.z, 2));
        return mag;
    }

    public double dotProduct(Vector3d vect1) {
        double dot = (this.x * vect1.getX()) + (this.y * vect1.getY()) + (this.z * vect1.getZ());
        return dot;
    }

    public Vector3d add(Vector3d vect1) {
        Vector3d newVect = new Vector3d(this.x + vect1.getX(), this.y + vect1.getY(), this.z + vect1.getZ());
        return newVect;
    }

}