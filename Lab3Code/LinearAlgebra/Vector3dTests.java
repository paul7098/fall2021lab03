package LinearAlgebra;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

public class Vector3dTests {

    @Test
    public void getXTest() {
        Vector3d vect1 = new Vector3d(1, 2, 3);
        assertEquals(1, vect1.getX(), 0);
    }

    @Test
    public void getYTest() {
        Vector3d vect1 = new Vector3d(1, 2, 3);
        assertEquals(2, vect1.getY(), 0);
    }

    @Test
    public void getZTest() {
        Vector3d vect1 = new Vector3d(1, 2, 3);
        assertEquals(3, vect1.getZ(), 0);
    }

    @Test
    public void magnitudeTest() {
        Vector3d vect1 = new Vector3d(1, 2, 3);
        assertEquals(3.74166, vect1.magnitude(), 0.00001);

    }

    @Test
    public void dotProductTest() {
        Vector3d vect1 = new Vector3d(1, 2, 3);
        Vector3d vect2 = new Vector3d(2, 3, 4);
        assertEquals(20, vect1.dotProduct(vect2), 0);

    }

    @Test
    public void addTestX() {
        Vector3d vect1 = new Vector3d(1, 2, 3);
        Vector3d vect2 = new Vector3d(2, 3, 4);
        Vector3d vect3 = vect1.add(vect2);
        assertEquals(3, vect3.getX(), 0);

    }

    @Test
    public void addTestY() {
        Vector3d vect1 = new Vector3d(1, 2, 3);
        Vector3d vect2 = new Vector3d(2, 3, 4);
        Vector3d vect3 = vect1.add(vect2);
        assertEquals(5, vect3.getY(), 0);

    }

    @Test
    public void addTestZ() {
        Vector3d vect1 = new Vector3d(1, 2, 3);
        Vector3d vect2 = new Vector3d(2, 3, 4);
        Vector3d vect3 = vect1.add(vect2);
        assertEquals(7, vect3.getZ(), 0);

    }

}
